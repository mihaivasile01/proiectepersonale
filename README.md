# README #

Proiect Twitter:
Proiect realizat in C++ drept proiect de laborator in cadrul materiei Programare Avansata in C++ 

Pentru rularea proiectului trebuie sa se dea build celor 3 proiecte, avand Solution Configuration setat pe RELEASE, platforma x64: Logging, TwitterPAC, TwitterPACClient

Proiect Competitie:
Proiect realizat in Java drept proiect de laborator in cadrul materiei Medii si instrumente de programare

Cerinta oferita de profesor:
La o competitie participa competitori. Fiecare competitor face parte dintr-o echipa (una si doar una). 
O echipa poate avea intre minim 2 si maxim 5 competitori. Administratorul competitiilor planifica etapele (la fiecare etapa participa competitori, nu echipe). 
La incheierea fiecarei etape, competitorii participanti isi inregistreaza un scor (care nu este obligatoriu sa fie unic fata de restul participantilor din etapa). 
Doar competitorul isi poate inregistra propriul scor (nu si administratorul) si nu poate fi modificat.
O etapa este considerata incheiata dupa ce toti competitorii inscrisi isi trec scorul aferent. 
Administratorul poate comunica candidatilor care nu si-au inscris scorul sa faca acest lucru.
Fiecare competitor, dar si administratorul, pot vedea clasamentul dupa etapele deja incheiate (cele care au inregistrare toate scorurile), individual si pe echipe. 
Intr-o etapa locul 1 primeste 10 puncte, locul 2, 6 puncte, locul 3, 3 puncte, locul 4, un punct. 
In cazul in care sunt mai multi competitori sunt pe acelasi loc fiecare va primi, media aritmetica a punctajului cumulat aferent locurilor.

Pentru rularea proiectului trebuie sa se dea build celor 2 proiecte, iar baza de date sa se afle in cadrul Serverului PostgreSQL14, baza de date avand numele "proiectmip"


Proiect Notepad++:
Proiect realizat in C#, implementand pattern-ul arhitectural MVVM, aplicatia putand fi rulata prin deschiderea sln-ului

Proiect Spanzuratoare:
Proiect realizat in C#, implementand MVVM, aplicatia putand fi rulata prin deschiderea sln-ului