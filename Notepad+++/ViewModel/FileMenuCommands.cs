﻿using Microsoft.Win32;
using Notepad___.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Input;

namespace Notepad___.ViewModel
{
    class FileMenuCommands : INotifyPropertyChanged
    {
        private int _currentSelectedTab;
        private int newFilesCounter;
        private string _currentSelectedItem;
        public string CurrentSelectedItem
        {
            get { return _currentSelectedItem; }
            set
            {
                _currentSelectedItem = value;
                OnPropertyChanged("CurrentSelectedItem");
            }
        }
        public int CurrentSelectedTab 
        { 
            get { return _currentSelectedTab; }
            set
            {
                _currentSelectedTab = value;
                OnPropertyChanged("CurrentSelectedTab");
            }
        }
        public ObservableCollection<FileTabProvider> FileTabs { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public AboutMenuCommands AboutTab { get; set; }
        public SearchMenuCommands SearchTab { get; set; }

        private ICommand m_openFile;
        private ICommand m_newFile;
        private ICommand m_closeFile;
        private ICommand m_saveAsFile;
        private ICommand m_saveFile;
        private ICommand m_openListView;
        private ICommand m_exitFile;

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void SaveFile(object parameter)
        {
            var tab = FileTabs[CurrentSelectedTab];

            if (tab.FileFullPath == "")
                SaveAsFile(parameter);
            else
                File.WriteAllText(tab.FileFullPath, tab.FileTabContent.ToString());

            tab.Save();
        }

        private void SaveAsFile(object parameter)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                var tab = FileTabs[CurrentSelectedTab];
                File.WriteAllText(saveFileDialog.FileName, FileTabs[CurrentSelectedTab].FileTabContent.ToString());
                tab.FileFullPath = saveFileDialog.FileName;
                tab.SetNewOriginal(Path.GetFileName(saveFileDialog.FileName));
                tab.Save();
            }
        }

        private void OpenFile(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                FileTabProvider openedFile= new FileTabProvider(Path.GetFileName(openFileDialog.FileName),openFileDialog.FileName, File.ReadAllText(openFileDialog.FileName));
                bool found = false;
                for(int iteratorTabs =0; iteratorTabs < FileTabs.Count; iteratorTabs++)
                    if(FileTabs[iteratorTabs].FileFullPath == openedFile.FileFullPath && FileTabs[iteratorTabs].FileTabName == openedFile.FileTabName)
                    {
                        found = true;
                        break;
                    }
                if (found == false)
                    FileTabs.Add(openedFile);
            }
        }

        private void OpenListViewFile(object parameter)
        {
            FileTabProvider openedFile = new FileTabProvider(Path.GetFileName(CurrentSelectedItem), CurrentSelectedItem, File.ReadAllText(CurrentSelectedItem));
            bool found = false;
            for (int iteratorTabs = 0; iteratorTabs < FileTabs.Count; iteratorTabs++)
                if (FileTabs[iteratorTabs].FileFullPath == openedFile.FileFullPath && FileTabs[iteratorTabs].FileTabName == openedFile.FileTabName)
                {
                    found = true;
                    break;
                }
            if (found == false)
                FileTabs.Add(openedFile);
        }

        private void NewFile(object parameter)
        {
            FileTabProvider openedFile = new FileTabProvider("File " + newFilesCounter.ToString(),"", "");
            newFilesCounter++;
            FileTabs.Add(openedFile);
        }

        private void ExitFile(object parameter)
        {
            for (int iteratorTabs = 0; iteratorTabs < FileTabs.Count; iteratorTabs++)
            {
                if(FileTabs[iteratorTabs].FileTabName.Contains('*'))
                    SaveFile(FileTabs[iteratorTabs]);
                CloseFile(FileTabs[iteratorTabs]);
            }
            System.Windows.Application.Current.Shutdown();
        }

        private void CloseFile(object parameter)
        {
            if (FileTabs[CurrentSelectedTab].FileTabName.Contains('*'))
                SaveFile(parameter);
            FileTabs.RemoveAt(CurrentSelectedTab);
        }

        public ICommand Exit
        {
            get
            {
                if (m_exitFile == null)
                    m_exitFile = new RelayCommand(ExitFile);
                return m_exitFile;
            }
        }
        public ICommand Save
        {
            get
            {
                if (m_saveFile == null)
                    m_saveFile = new RelayCommand(SaveFile);
                return m_saveFile;
            }
        }

        public ICommand SaveAs
        {
            get
            {
                if (m_saveAsFile == null)
                    m_saveAsFile = new RelayCommand(SaveAsFile);
                return m_saveAsFile;
            }
        }

        public ICommand OpenListView
        {
            get
            {
                if (m_openListView == null)
                    m_openListView = new RelayCommand(OpenListViewFile);
                return m_openListView;
            }
        }

        public ICommand Open
        {
            get
            {
                if (m_openFile == null)
                    m_openFile = new RelayCommand(OpenFile);
                return m_openFile;
            }
        }

        public ICommand New
        {
            get
            {
                if (m_newFile == null)
                    m_newFile = new RelayCommand(NewFile);
                return m_newFile;
            }
        }

        public ICommand Close
        {
            get
            {
                if (m_closeFile == null)
                    m_closeFile = new RelayCommand(CloseFile);
                return m_closeFile;
            }
        }

        public FileMenuCommands()
        {
            FileTabs = new ObservableCollection<FileTabProvider>();
            AboutTab = new AboutMenuCommands();
            SearchTab = new SearchMenuCommands();

            newFilesCounter = 0;
        }

    }


}
