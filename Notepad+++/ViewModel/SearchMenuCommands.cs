﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Notepad___.View;
using Notepad___.ViewModel;

namespace Notepad___.ViewModel
{
    class SearchMenuCommands : INotifyPropertyChanged
    {
        public string FindSequence { get; set; }
        public string ReplaceSequence { get; set; }
        public string ReplaceAllSequence { get; set; }

        public string ToReplaceSequence { get; set; }
        public string ToReplaceAllSequence { get; set; }

        public bool ReplaceOption { get; set; }
        public bool ReplaceAllOption { get; set; }

        public bool FindOption { get; set; }

        private ICommand m_findInFile;
        private ICommand m_replaceInFile;
        private ICommand m_replaceAllInFile;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void FindInFile(object parameter)
        {
            FileMenuCommands menuCommands = (FileMenuCommands)parameter;
            var findPage = new FindPage();
            findPage.ShowDialog();
            if (findPage.FindOption.IsChecked == false)
            {
                int startPos = 0;
                int index = menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent.IndexOf(findPage.FindSequence.Text, startPos);
                while (index != -1)
                {
                    menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent = 
                        menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent.Insert(index, "$");
                    menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent= 
                        menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent.Insert(index + findPage.FindSequence.Text.Length+1, "$");
                    startPos = index + findPage.FindSequence.Text.Length;
                    index = menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent.IndexOf(findPage.FindSequence.Text, startPos);
                }
            }
            else
            {
                for (int iterator = 0; iterator < menuCommands.FileTabs.Count; iterator++)
                {
                    int startPos = 0;
                    int index = menuCommands.FileTabs[iterator].FileTabContent.IndexOf(findPage.FindSequence.Text, startPos);
                    while(index != -1)
                    {
                        menuCommands.FileTabs[iterator].FileTabContent = menuCommands.FileTabs[iterator].FileTabContent.Insert(index, "$");
                        menuCommands.FileTabs[iterator].FileTabContent = menuCommands.FileTabs[iterator].FileTabContent.Insert(index + findPage.FindSequence.Text.Length + 1, "$");
                        startPos = index + findPage.FindSequence.Text.Length;
                        index = menuCommands.FileTabs[iterator].FileTabContent.IndexOf(findPage.FindSequence.Text, startPos);
                    }
                }
            }
        }

        private void ReplaceAllInFile(object parameter)
        {
            FileMenuCommands menuCommands = (FileMenuCommands)parameter;
            var replaceAllPage = new ReplaceAllPage();
            replaceAllPage.ShowDialog();
            if(replaceAllPage.ReplaceAllOption.IsChecked == false)
            {
                menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent =
                new string(menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent.Replace(replaceAllPage.ToReplaceAllSequence.Text,
                replaceAllPage.ReplaceAllSequence.Text));
            }
            else
            {
                for (int iterator = 0; iterator < menuCommands.FileTabs.Count; iterator++)
                    menuCommands.FileTabs[iterator].FileTabContent =
                        new string(menuCommands.FileTabs[iterator].FileTabContent.Replace(replaceAllPage.ToReplaceAllSequence.Text,
                        replaceAllPage.ReplaceAllSequence.Text));
            }
        }
        private void ReplaceInFile(object parameter)
        {
            FileMenuCommands menuCommands = (FileMenuCommands)parameter;
            var replacePage = new ReplacePage();
            replacePage.ShowDialog();
            if(replacePage.ReplaceOption.IsChecked == false)
            {
                Regex regex = new Regex(replacePage.ToReplaceSequence.Text);
                menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent =
                    regex.Replace(menuCommands.FileTabs[menuCommands.CurrentSelectedTab].FileTabContent, replacePage.ReplaceSequence.Text, 1);
            }
            else
            {
                for (int iterator = 0; iterator < menuCommands.FileTabs.Count; iterator++)
                {
                    Regex regex = new Regex(replacePage.ToReplaceSequence.Text);
                    menuCommands.FileTabs[iterator].FileTabContent =
                        regex.Replace(menuCommands.FileTabs[iterator].FileTabContent, replacePage.ReplaceSequence.Text, 1);
                }
            }
        }

        public ICommand ReplaceAll
        {
            get
            {
                if (m_replaceAllInFile == null)
                    m_replaceAllInFile = new RelayCommand(ReplaceAllInFile);
                return m_replaceAllInFile;
            }
        }
        public ICommand Replace
        {
            get
            {
                if (m_replaceInFile == null)
                    m_replaceInFile = new RelayCommand(ReplaceInFile);
                return m_replaceInFile;
            }
        }
        public ICommand Find
        {
            get
            {
                if (m_findInFile == null)
                    m_findInFile = new RelayCommand(FindInFile);
                return m_findInFile;
            }
        }
    }
}
