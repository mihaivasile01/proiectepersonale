﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Notepad___.Model
{
    class FileTabProvider : INotifyPropertyChanged
    {
        private string _originalFileTabName;

        private string _fileTabName;
        public string FileTabName
        {
            get { return _fileTabName; }
            set { _fileTabName = value; OnPropertyChanged(nameof(FileTabName)); }
        }

        public string FileFullPath { get; set; }

        private string _fileTabContent;
        public string FileTabContent
        {
            get { return _fileTabContent; }
            set
            {
                _fileTabContent = value;
                if (!FileTabName.Contains('*'))
                    FileTabName += "*";
                OnPropertyChanged(nameof(FileTabContent));
            }
        }

        public FileTabProvider(string fileTabName, string fileFullPath, string fileTabContent)
        {
            _originalFileTabName = fileTabName;
            _fileTabName = fileTabName;
            FileFullPath = fileFullPath;
            _fileTabContent = fileTabContent;
        }

        public void SetNewOriginal(string newOriginal) => _originalFileTabName = newOriginal;
        public void Save() => FileTabName = _originalFileTabName;

        public event PropertyChangedEventHandler? PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
