package socket;

import database.daos.EchipaDAO;
import database.daos.EtapaDAO;
import database.daos.ParticipantDAO;
import database.daos.PersoanaDAO;
import database.models.EchipaEntity;
import database.models.EtapaEntity;
import database.models.ParticipantEntity;
import database.models.PersoanaEntity;
import jakarta.persistence.NoResultException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Stream;

import static socket.Server.messageLibrary;

public class InterpretData {
    void processData (JSONObject data, ObjectOutputStream outputStream) {
        switch (data.get("command").toString()) {
            case "LoginRequest": {
                PersoanaDAO persoanaDao = new PersoanaDAO();
                String username = data.get("username").toString();
                JSONObject json = new JSONObject();
                try {
                    PersoanaEntity persoanaEntity = persoanaDao.get(username);
                    json.put("resultExecution", "success");
                    json.put("isAdmin", persoanaEntity.isAdmin());
                } catch(Exception e) {
                    json.put("resultExecution", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "CreateUser": {
                PersoanaDAO persoanaDAO= new PersoanaDAO();
                JSONObject jsonObject = new JSONObject();
                try {
                    PersoanaEntity toCreate = new PersoanaEntity();
                    EchipaDAO echipaDAO = new EchipaDAO();
                    Boolean isAdmin = (Boolean) data.get("isAdmin");
                    if(!isAdmin)
                    {
                        EchipaEntity echipaEntity = echipaDAO.get(data.get("numeEchipa").toString());
                        toCreate.setEchipaEntity(echipaEntity);
                    }
                    toCreate.setUsername(data.get("username").toString());
                    toCreate.setNume(data.get("nume").toString());
                    toCreate.setAdmin(isAdmin);
                    persoanaDAO.save(toCreate);
                    jsonObject.put("status_code", "success");
                } catch(Exception e) {
                    System.out.println(e.toString());
                    jsonObject.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(jsonObject.toString());
                } catch (IOException e) {
                    System.out.println(e.toString());
                }
                break;
            }
            case "CreateTeam": {
                EchipaDAO echipaDao = new EchipaDAO();
                JSONObject json = new JSONObject();
                try {
                    EchipaEntity echipaEntity = new EchipaEntity();
                    echipaEntity.setNumeEchipa(data.get("numeEchipa").toString());
                    echipaDao.save(echipaEntity);
                    json.put("status_code", "success");
                } catch(Exception e) {
                    System.out.println(e.toString());
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "CreateStage": {
                EtapaDAO etapaDao = new EtapaDAO();
                JSONObject json = new JSONObject();
                try {
                    EtapaEntity etapaEntity = new EtapaEntity();
                    etapaEntity.setDenumire(data.get("numeEtapa").toString());
                    etapaEntity.setIncheiata(false);
                    etapaDao.save(etapaEntity);
                    json.put("status_code", "success");
                } catch(Exception e) {
                    System.out.println(e.toString());
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "GetTeams": {
                PersoanaDAO persoanaDao = new PersoanaDAO();
                String username = data.get("username").toString();
                PersoanaEntity persoanaEntity = persoanaDao.get(username);
                JSONObject json = new JSONObject();
                if(persoanaEntity.isAdmin())
                {
                    EchipaDAO echipaDao = new EchipaDAO();
                    List<EchipaEntity> echipe = echipaDao.getAll();
                    for(EchipaEntity echipa : echipe)
                    {
                        json.append("numeEchipa", echipa.getNumeEchipa());
                    }
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "GetStages": {
                EtapaDAO etapaDAO = new EtapaDAO();
                JSONObject json = new JSONObject();
                List<EtapaEntity> listaEtape = etapaDAO.getUnfinished();
                for(EtapaEntity etapa : listaEtape)
                {
                    json.append("numeEtapa",etapa.getDenumire());
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "GetStagesForScore":
            {
                ParticipantDAO participantDao = new ParticipantDAO();
                PersoanaDAO persoanaDao = new PersoanaDAO();
                String username = data.get("username").toString();
                PersoanaEntity persoanaEntity = persoanaDao.get(username);
                JSONObject json = new JSONObject();
                List<ParticipantEntity> participanti = participantDao.getAllWithUserID(persoanaEntity);
                for(ParticipantEntity participant : participanti)
                {
                    EtapaEntity etapaEntity = participant.getEtapaEntity();
                    if(Objects.equals(etapaEntity.getIncheiata(), true))
                    {
                        json.append("numeEtapa", etapaEntity.getDenumire());
                    }
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "FinishStage": {
                EtapaDAO etapaDao = new EtapaDAO();
                JSONObject json = new JSONObject();
                try {
                    EtapaEntity etapaEntity = etapaDao.get(data.get("numeEtapa").toString());
                    etapaEntity.setIncheiata(true);
                    etapaDao.update(etapaEntity);
                    json.put("status_code", "success");
                } catch(Exception e) {
                    System.out.println(e.toString());
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "RegisterUserToStage": {
                JSONObject json = new JSONObject();
                try {
                    EtapaDAO etapaDao = new EtapaDAO();
                    PersoanaDAO persoanaDao = new PersoanaDAO();
                    ParticipantDAO participantDao = new ParticipantDAO();
                    EtapaEntity etapaEntity = etapaDao.get(data.get("numeEtapa").toString());
                    PersoanaEntity persoanaEntity = persoanaDao.get(data.get("username").toString());
                    ParticipantEntity participantEntity = new ParticipantEntity();
                    participantEntity.setPersoanaEntity(persoanaEntity);
                    participantEntity.setEtapaEntity(etapaEntity);
                    participantDao.save(participantEntity);
                    json.put("status_code", "success");
                } catch(Exception e) {
                    System.out.println(e.toString());
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "SaveMessage": {
                JSONObject json = new JSONObject();
                PersoanaDAO persoanaDao = new PersoanaDAO();
                String username = data.get("username").toString();
                try {
                    persoanaDao.get(username);
                    try {
                        messageLibrary.put(username, data.get("message").toString());
                        json.put("status_code", "success");
                    } catch (Exception e) {
                        json.put("status_code", "failure");
                    }
                } catch (NoResultException e) {
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "CheckMessage": {
                JSONObject json = new JSONObject();
                PersoanaDAO persoanaDao = new PersoanaDAO();
                String mesaj;
                try {
                    String username = data.get("username").toString();
                    mesaj = messageLibrary.get(username);
                    if(mesaj == null)
                    {
                        throw new NoResultException();
                    }
                    messageLibrary.remove(username);
                    json.put("status_code", "success");
                    json.put("message", mesaj);
                } catch (NoResultException e) {
                    json.put("status_code", "failure");
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "ShowIndividualRanking": {
                ParticipantDAO participantDao = new ParticipantDAO();
                EtapaDAO etapaDao = new EtapaDAO();
                List<EtapaEntity> stages = etapaDao.getFinishedAndFilled();
                Map<String, Integer> scores = new HashMap<>();
                for(EtapaEntity stage : stages)
                {
                    List<ParticipantEntity> participantsStage= participantDao.getAllFromStage(stage);
                    for (ParticipantEntity participant : participantsStage)
                    {
                        scores.put(participant.getPersoanaEntity().getUsername(),participant.getPunctaj());
                    }
                }
                Stream<Map.Entry<String, Integer>> sorted =
                        scores.entrySet().stream()
                                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()));
                JSONObject json = new JSONObject();
                json.put("leaderboard", sorted);
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            case "RegisterScoreStage":
            {
                JSONObject json = new JSONObject();
                json.put("status_code", "success");
                try {
                    ParticipantDAO participantDao = new ParticipantDAO();
                    PersoanaDAO persoanaDao = new PersoanaDAO();
                    EtapaDAO etapaDao = new EtapaDAO();
                    PersoanaEntity persoanaEntity = persoanaDao.get(data.get("username").toString());
                    EtapaEntity etapaEntity = etapaDao.get(data.get("numeEtapa").toString());
                    ParticipantEntity participant = new ParticipantEntity();
                    participant.setPunctaj(Integer.parseInt(data.get("score").toString()));
                    participant.setEtapaEntity(etapaEntity);
                    participant.setPersoanaEntity(persoanaEntity);
                    participantDao.update(participant);
                } catch (Exception e) {
                    json.put("status_code", "failure");
                    e.printStackTrace();
                }
                try {
                    outputStream.writeObject(json.toString());
                } catch (IOException e)
                {
                    System.out.println(e.toString());
                }
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + data.get("command").toString());
        }
    }
}