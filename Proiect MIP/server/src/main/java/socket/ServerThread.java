package socket;
import org.json.JSONObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread extends Thread{
    private Socket socket = null;
    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;

    public ServerThread(Socket socket) {
        this.socket = socket;
        try {
            //For receiving and sending data
            this.in = new ObjectInputStream(socket.getInputStream());
            this.out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //The isInputShutdown () method of Java Socket class returns a Boolean value 'true'
    // if the read-half of the socket connection has been closed, else it returns false.
    public void run() {
        while(!socket.isInputShutdown()) {
            try {
                String received = in.readObject().toString();
                JSONObject jsonObject = new JSONObject(received);
                execute(received);
            } catch (IOException e) {
                System.out.println("A client connection has been closed");
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    private void execute(String stringStream) {
        JSONObject jsonObject = new JSONObject(stringStream);
        InterpretData dataInterpret = new InterpretData();
        dataInterpret.processData(jsonObject,this.out);
    }
}