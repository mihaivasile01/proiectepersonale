import database.daos.EchipaDAO;
import database.daos.PersoanaDAO;
import database.models.EchipaEntity;
import database.models.PersoanaEntity;
import socket.Server;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Server socket = new Server();
        socket.start();
    }
}
