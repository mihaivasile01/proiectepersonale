package database.daos;

import database.DatabaseConnection;
import database.models.EchipaEntity;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

import java.util.List;
import java.util.Optional;

public class EchipaDAO implements IDao<EchipaEntity>{
    DatabaseConnection connection = new DatabaseConnection();

    @Override
    public Optional<EchipaEntity> get(long id) {
        return Optional.ofNullable(connection.getEntityManager().find(EchipaEntity.class,id));
    }

    public EchipaEntity get(String numeEchipa) throws NoResultException {
        return connection
                .getEntityManager()
                .createQuery("SELECT a FROM EchipaEntity as a WHERE a.numeEchipa = :numeEchipa", EchipaEntity.class)
                .setParameter("numeEchipa", numeEchipa).getSingleResult();
    }

    @Override
    public List<EchipaEntity> getAll() {
        TypedQuery<EchipaEntity> typedQuery =
                connection.getEntityManager().createQuery("SELECT u FROM EchipaEntity as u",EchipaEntity.class);
        return typedQuery.getResultList();
    }

    @Override
    public void save(EchipaEntity echipa) {
        connection.executeTransaction(entityManager -> entityManager.persist(echipa));
    }

    @Override
    public void update(EchipaEntity echipa) {
        connection.executeTransaction(entityManager -> entityManager.merge(echipa));
    }

    @Override
    public void delete(EchipaEntity echipa) {
        connection.executeTransaction(entityManager -> entityManager.remove(echipa));
    }
}
