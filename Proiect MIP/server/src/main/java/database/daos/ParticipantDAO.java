package database.daos;

import database.DatabaseConnection;
import database.models.EtapaEntity;
import database.models.ParticipantEntity;
import database.models.PersoanaEntity;
import jakarta.persistence.*;

import java.util.*;

public class ParticipantDAO implements IDao<ParticipantEntity> {
    DatabaseConnection databaseConnection = new DatabaseConnection();

    @Override
    public Optional<ParticipantEntity> get(long id) {
        return Optional.ofNullable(databaseConnection.getEntityManager().find(ParticipantEntity.class,id));
    }

    @Override
    public List<ParticipantEntity> getAll() {
        TypedQuery<ParticipantEntity> typedQuery =
                databaseConnection.getEntityManager().createQuery("SELECT u FROM ParticipantEntity as u", ParticipantEntity.class);
        return typedQuery.getResultList();
    }

    public List<ParticipantEntity> getAllFromStage(EtapaEntity etapa) {
        TypedQuery<ParticipantEntity> typedQuery=
                databaseConnection.getEntityManager()
                        .createQuery("SELECT a FROM ParticipantEntity AS a WHERE a.etapaEntity = :etapa ORDER BY a.punctaj DESC",ParticipantEntity.class)
                        .setParameter("etapa",etapa);
        return typedQuery.getResultList();
    }

    public List<ParticipantEntity> getAllWithUserID(PersoanaEntity persoanaEntity) {
        TypedQuery<ParticipantEntity> query =
                databaseConnection.getEntityManager()
                .createQuery("SELECT a FROM ParticipantEntity as a " +
                                "WHERE a.persoanaEntity = :persoanaEntity AND a.punctaj = null"
                        , ParticipantEntity.class)
                .setParameter("persoanaEntity", persoanaEntity);
        return query.getResultList();
    }

    @Override
    public void save(ParticipantEntity participant) {
        databaseConnection.executeTransaction(entityManager -> entityManager.persist(participant));
    }

    @Override
    public void update(ParticipantEntity participant) {
        databaseConnection.executeTransaction(entityManager -> entityManager.merge(participant));
    }

    @Override
    public void delete(ParticipantEntity participant) {
        databaseConnection.executeTransaction(entityManager -> entityManager.remove(participant));
    }
}
