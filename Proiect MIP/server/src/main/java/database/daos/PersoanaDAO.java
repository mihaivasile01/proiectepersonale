package database.daos;

import database.DatabaseConnection;
import database.models.PersoanaEntity;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

import java.util.List;
import java.util.Optional;

public class PersoanaDAO implements IDao<PersoanaEntity> {
    DatabaseConnection databaseConnection = new DatabaseConnection();

    @Override
    public Optional<PersoanaEntity> get(long id) {
        return Optional.ofNullable(databaseConnection.getEntityManager().find(PersoanaEntity.class,id));
    }

    public PersoanaEntity get(String username) throws NoResultException {
        return databaseConnection.getEntityManager()
                .createQuery("SELECT p FROM PersoanaEntity AS p WHERE p.username = :username", PersoanaEntity.class)
                .setParameter("username",username).getSingleResult();
    }

    @Override
    public List<PersoanaEntity> getAll() {
        TypedQuery<PersoanaEntity> typedQuery =
                databaseConnection.getEntityManager().createQuery("SELECT u FROM PersoanaEntity AS u",PersoanaEntity.class);
        return typedQuery.getResultList();
    }

    @Override
    public void save(PersoanaEntity persoana) {
        databaseConnection.executeTransaction(entityManager -> entityManager.persist(persoana));
    }

    @Override
    public void update(PersoanaEntity persoana) {
        databaseConnection.executeTransaction(entityManager -> entityManager.merge(persoana));
    }

    @Override
    public void delete(PersoanaEntity persoana) {
        databaseConnection.executeTransaction(entityManager -> entityManager.remove(persoana));
    }
}
