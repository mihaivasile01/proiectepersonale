package database.daos;

import database.DatabaseConnection;
import database.models.EtapaEntity;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

import java.util.List;
import java.util.Optional;

public class EtapaDAO implements IDao<EtapaEntity> {
    DatabaseConnection connection = new DatabaseConnection();
    @Override
    public Optional<EtapaEntity> get(long id) {
        return Optional.ofNullable(connection.getEntityManager().find(EtapaEntity.class,id));
    }

    public EtapaEntity get(String numeEtapa) throws NoResultException {
        return connection
                .getEntityManager()
                .createQuery("SELECT a FROM EtapaEntity as a WHERE a.denumire = :denumire", EtapaEntity.class)
                .setParameter("denumire", numeEtapa).getSingleResult();
    }

    @Override
    public List<EtapaEntity> getAll() {
        TypedQuery<EtapaEntity> typedQuery =
                connection.getEntityManager().createQuery("SELECT u FROM EtapaEntity AS u",EtapaEntity.class);
        return typedQuery.getResultList();
    }

    public List<EtapaEntity> getUnfinished() {
        TypedQuery<EtapaEntity> typedQuery =
                connection.getEntityManager()
                        .createQuery("SELECT u FROM EtapaEntity AS u WHERE u.incheiata=false",EtapaEntity.class);
        return typedQuery.getResultList();
    }

    public List<EtapaEntity> getFinished() {
        TypedQuery<EtapaEntity> typedQuery =
                connection.getEntityManager()
                .createQuery("SELECT a FROM EtapaEntity as a WHERE a.incheiata = true", EtapaEntity.class);
        return typedQuery.getResultList();
    }

    public List<EtapaEntity> getFinishedAndFilled() {
        TypedQuery<EtapaEntity> typedQuery =
                connection.getEntityManager().createQuery("SELECT u FROM EtapaEntity AS u " +
                        " GROUP BY u.idEtapa HAVING (SELECT COUNT(p.etapaEntity.idEtapa) FROM ParticipantEntity AS p " +
                                " WHERE p.etapaEntity=u AND p.punctaj IS NULL) = 0 AND u.incheiata=true",EtapaEntity.class);
        return typedQuery.getResultList();
    }

    @Override
    public void save(EtapaEntity etapa) {
        connection.executeTransaction(entityManager -> entityManager.persist(etapa));
    }

    @Override
    public void update(EtapaEntity etapa) {
        connection.executeTransaction(entityManager -> entityManager.merge(etapa));
    }

    @Override
    public void delete(EtapaEntity etapa) {
        connection.executeTransaction(entityManager -> entityManager.remove(etapa));
    }
}
