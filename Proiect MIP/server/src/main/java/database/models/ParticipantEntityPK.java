package database.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Id;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ParticipantEntityPK implements Serializable {
    private static final long serialVersionUID = 1536000815201108503L;
    @Column(name = "id_persoana", nullable = false, insertable = false, updatable = false)
    private long idPersoana;
    @Column(name = "id_etapa", nullable = false, insertable = false, updatable = false)
    private long idEtapa;

    public long getIdPersoana() {
        return idPersoana;
    }

    public void setIdPersoana(long idPersoana) {
        this.idPersoana = idPersoana;
    }

    public long getIdEtapa() {
        return idEtapa;
    }

    public void setIdEtapa(long idEtapa) {
        this.idEtapa = idEtapa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantEntityPK that = (ParticipantEntityPK) o;
        return idPersoana == that.idPersoana && idEtapa == that.idEtapa;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPersoana, idEtapa);
    }
}
