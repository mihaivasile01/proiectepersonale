package database.models;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "echipa", schema = "public", catalog = "proiectmip")
public class EchipaEntity {
    @Column(name = "nume_echipa", nullable = false, unique = true, length = -1)
    private String numeEchipa;
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "echipaEntity", orphanRemoval = true)
    private List<PersoanaEntity> persoanaEntities = new ArrayList<>();

    public List<PersoanaEntity> getPersoanaEntities() {
        return persoanaEntities;
    }

    public void setPersoanaEntities(List<PersoanaEntity> persoanaEntities) {
        this.persoanaEntities = persoanaEntities;
    }

    public String getNumeEchipa() {
        return numeEchipa;
    }

    public void setNumeEchipa(String numeEchipa) {
        this.numeEchipa = numeEchipa;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EchipaEntity that = (EchipaEntity) o;
        return id == that.id && Objects.equals(numeEchipa, that.numeEchipa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numeEchipa, id);
    }
}
