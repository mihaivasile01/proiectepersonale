package database.models;

import jakarta.persistence.*;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "persoana", schema = "public", catalog = "proiectmip")
public class PersoanaEntity {
    @Column(name = "username", nullable = false, unique = true, length = -1)
    private String username;
    @Basic
    @Column(name = "isadmin", nullable = false)
    private boolean isAdmin;
    @Basic
    @Column(name = "nume", nullable = true, length = -1)
    private String nume;
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "id_echipa", nullable = true)
    private EchipaEntity echipaEntity;

    @OneToMany(mappedBy = "persoanaEntity", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<ParticipantEntity> participantEntities = new LinkedHashSet<>();

    public Set<ParticipantEntity> getParticipantEntities() {
        return participantEntities;
    }

    public void setParticipantEntities(Set<ParticipantEntity> participantEntities) {
        this.participantEntities = participantEntities;
    }

    public EchipaEntity getEchipaEntity() {
        return echipaEntity;
    }

    public void setEchipaEntity(EchipaEntity echipaEntity) {
        this.echipaEntity = echipaEntity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersoanaEntity that = (PersoanaEntity) o;
        return isAdmin == that.isAdmin && id == that.id && Objects.equals(username, that.username) && Objects.equals(nume, that.nume);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, isAdmin, nume, id);
    }
}
