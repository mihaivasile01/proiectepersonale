package database.models;

import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "etapa", schema = "public", catalog = "proiectmip")
public class EtapaEntity {
    @Basic
    @Column(name = "incheiata", nullable = false)
    private boolean incheiata;
    @Column(name = "denumire", nullable = false, unique = true, length = -1)
    private String denumire;
    @Id
    @Column(name = "id_etapa", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idEtapa;

    @OneToMany(mappedBy = "etapaEntity", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<ParticipantEntity> participantEntities = new LinkedHashSet<>();

    public Set<ParticipantEntity> getParticipantEntities() {
        return participantEntities;
    }

    public void setParticipantEntities(Set<ParticipantEntity> participantEntities) {
        this.participantEntities = participantEntities;
    }

    public boolean getIncheiata() {
        return incheiata;
    }

    public void setIncheiata(boolean incheiata) {
        this.incheiata = incheiata;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public long getIdEtapa() {
        return idEtapa;
    }

    public void setIdEtapa(long idEtapa) {
        this.idEtapa = idEtapa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EtapaEntity that = (EtapaEntity) o;
        return idEtapa == that.idEtapa && Objects.equals(incheiata, that.incheiata) && Objects.equals(denumire, that.denumire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(incheiata, denumire, idEtapa);
    }
}
