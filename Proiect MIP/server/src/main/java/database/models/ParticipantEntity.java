package database.models;

import jakarta.persistence.*;

@Entity
@Table(name = "participant", schema = "public", catalog = "proiectmip")
public class ParticipantEntity {
    @EmbeddedId
    private ParticipantEntityPK id;
    @Basic
    @Column(name = "punctaj", nullable = true)
    private Integer punctaj;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_etapa")
    private EtapaEntity etapaEntity;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_persoana")
    private PersoanaEntity persoanaEntity;

    public PersoanaEntity getPersoanaEntity() {
        return persoanaEntity;
    }

    public void setPersoanaEntity(PersoanaEntity persoanaEntity) {
        this.persoanaEntity = persoanaEntity;
    }

    public EtapaEntity getEtapaEntity() {
        return etapaEntity;
    }

    public void setEtapaEntity(EtapaEntity etapaEntity) {
        this.etapaEntity = etapaEntity;
    }

    public Integer getPunctaj() {
        return punctaj;
    }

    public void setPunctaj(Integer punctaj) {
        this.punctaj = punctaj;
    }

    public ParticipantEntityPK getId() {
        return id;
    }

    public void setId(ParticipantEntityPK id) {
        this.id = id;
    }
}
