package com.example.demo;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.json.JSONObject;
import socket.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class UserController {

    @FXML
    private Text stageSignUp;

    @FXML
    private Text notificationRed;

    @FXML
    private Text notificationBody;

    @FXML
    private Text scoreInputResult;

    @FXML
    private TextField scoreInputField;

    @FXML
    private Button logoutButton;

    @FXML
    private Button registerUserToStageButton;

    @FXML
    private Button registerScoreButton;

    @FXML
    private ChoiceBox finishedStageChoice;

    @FXML
    private ChoiceBox stageChoice;

    @FXML
    public void initialize() {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        String received;
        JSONObject receivedJSON = null;

        json.put("command", "CheckMessage");
        json.put("username", client.getName());
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error getting message");
        }
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            if(receivedJSON.get("status_code").toString().equals("success")) {
                notificationBody.setText(receivedJSON.get("message").toString());
                notificationRed.setVisible(true);
                notificationBody.setVisible(true);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        json.put("command","GetStages");
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error getting stages");
        }
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            stageChoice.setItems(FXCollections.observableArrayList(receivedJSON.getJSONArray("numeEtapa").toList()));
        }
        catch (Exception e)
        {
            stageChoice.setItems(FXCollections.observableArrayList(""));
        }
        json.put("command", "GetStagesForScore");
        json.put("username", client.getName());
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error getting finalized stages");
        }
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            finishedStageChoice.setItems(FXCollections.observableArrayList(receivedJSON.getJSONArray("numeEtapa").toList()));
        }
        catch (Exception e)
        {
            finishedStageChoice.setItems(FXCollections.observableArrayList(""));
        }
    }

    @FXML
    public void onLogoutButtonClick(MouseEvent mouseEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainController.class.getResource("main-view.fxml"));
        Parent submitted = (Parent) fxmlLoader.load();

        Client client = Client.INSTANCE;

        Window window = ((Node) mouseEvent.getTarget()).getScene().getWindow();
        double height = 800;
        double width = 800;
        double x = window.getX();
        double y = window.getY();
        boolean isFullscreen = ((Stage) window).isFullScreen();

        Scene scene = new Scene(submitted);
        Stage appStage = (Stage) window;
        appStage.setScene(scene);

        appStage.setHeight(height);
        appStage.setWidth(width);
        appStage.setX(x);
        appStage.setY(y);
        appStage.setFullScreen(isFullscreen);

        appStage.show();
        client.setName("");
        return;
    }

    @FXML
    public void registerUserToStage(MouseEvent mouseEvent) throws IOException{
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "RegisterUserToStage");
        json.put("username", client.getName());
        json.put("numeEtapa", stageChoice.getValue());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error registering user to a stage");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            stageSignUp.setText("Error registering to a stage");
            stageSignUp.setVisible(true);
            return;
        }
        javafx.collections.ObservableList availableStages = stageChoice.getItems();
        availableStages.remove(stageChoice.getValue());
        stageChoice.setItems(availableStages);
        stageSignUp.setText("Successfully registered into a stage");
        stageSignUp.setVisible(true);
    }

    @FXML
    public void onClickRegisterScore(MouseEvent mouseEvent) {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "RegisterScoreStage");
        json.put("username", client.getName());
        json.put("numeEtapa", finishedStageChoice.getValue());
        json.put("score", scoreInputField.getText());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error registering score");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            scoreInputResult.setText("Error registering score");
            scoreInputResult.setVisible(true);
            return;
        }
        javafx.collections.ObservableList etapeTerminateDisponibile = finishedStageChoice.getItems();
        etapeTerminateDisponibile.remove(finishedStageChoice.getValue());
        finishedStageChoice.setItems(etapeTerminateDisponibile);
        scoreInputResult.setText("Score saved successfully");
        scoreInputResult.setVisible(true);
    }
}
