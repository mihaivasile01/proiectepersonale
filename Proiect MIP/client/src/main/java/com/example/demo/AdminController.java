package com.example.demo;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.json.JSONObject;
import socket.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class AdminController {
    @FXML
    private Button logoutButton;

    @FXML
    private Button createTeamButton;

    @FXML
    private Button createUserButton;

    @FXML
    private Button createStageButton;

    @FXML
    private Button closeStageButton;

    @FXML
    private Button sendMessageButton;

    @FXML
    private Text userCreation;

    @FXML
    private Text stageCreation;

    @FXML
    private Text userCreateSection;

    @FXML
    private Text teamCreation;

    @FXML
    private Text closedStage;

    @FXML
    private Text messageSentCheck;

    @FXML
    private TextField nameField;

    @FXML
    private TextField usernameField;

    @FXML
    private TextField teamNameField;

    @FXML
    private TextField stageNameField;

    @FXML
    private TextArea messageBodyField;

    @FXML
    private TextField usernameToSendField;

    @FXML
    private CheckBox isAdminCheckBox;

    @FXML
    private ChoiceBox teamChoice;

    @FXML
    private ChoiceBox stageChoice;

    @FXML
    public void initialize() {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "GetTeams");
        json.put("username", client.getName());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error getting teams");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            teamChoice.setItems(FXCollections.observableArrayList(receivedJSON.getJSONArray("numeEchipa").toList()));
        }
        catch (Exception e)
        {
            teamChoice.setItems(FXCollections.observableArrayList(""));
        }
        json.put("command", "GetStages");
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error getting stages");
        }
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            stageChoice.setItems(FXCollections.observableArrayList(receivedJSON.getJSONArray("numeEtapa").toList()));
        }
        catch (Exception e)
        {
            stageChoice.setItems(FXCollections.observableArrayList(""));
        }
    }

    @FXML
    public void onLogoutButtonClick(MouseEvent mouseEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainController.class.getResource("main-view.fxml"));
        Parent submitted = (Parent) fxmlLoader.load();

        Client client = Client.INSTANCE;

        Window window = ((Node) mouseEvent.getTarget()).getScene().getWindow();
        double height = 800;
        double width = 800;
        double x = window.getX();
        double y = window.getY();
        boolean isFullscreen = ((Stage) window).isFullScreen();

        Scene scene = new Scene(submitted);
        Stage appStage = (Stage) window;
        appStage.setScene(scene);

        appStage.setHeight(height);
        appStage.setWidth(width);
        appStage.setX(x);
        appStage.setY(y);
        appStage.setFullScreen(isFullscreen);

        appStage.show();
        client.setName("");
        return;
    }

    @FXML
    public void onCreateUser(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "CreateUser");
        json.put("nume", nameField.getText());
        json.put("username", usernameField.getText());
        json.put("isAdmin", isAdminCheckBox.isSelected());
        json.put("numeEchipa", teamChoice.getValue().toString());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error creating user");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            userCreation.setText("User couldn't be created !");
            userCreation.setVisible(true);
        }
        else
        {
            userCreation.setText("User successfully created!");
            userCreation.setVisible(true);
        }
    }

    @FXML
    public void onCreateTeam(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "CreateTeam");
        json.put("numeEchipa", teamNameField.getText());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error creating the team");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if (receivedJSON.get("status_code").toString().equals("failure")) {
            teamCreation.setText("Team couldn't be created");
            teamCreation.setVisible(true);
        } else {
            javafx.collections.ObservableList obsArrayList = teamChoice.getItems();
            obsArrayList.add(teamNameField.getText());
            teamChoice.setItems(obsArrayList);
            teamCreation.setText("Team created successfully");
            teamCreation.setVisible(true);
        }
    }

    @FXML
    public void onCreateStage(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "CreateStage");
        json.put("numeEtapa", stageNameField.getText());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error creating the stage");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            stageCreation.setText("The stage couldn't be created");
            stageCreation.setVisible(true);
        }
        else
        {
            javafx.collections.ObservableList obsArrayList = stageChoice.getItems();
            obsArrayList.add(stageNameField.getText());
            stageChoice.setItems(obsArrayList);
            stageCreation.setText("Stage created successfully");
            stageCreation.setVisible(true);
        }
    }

    @FXML
    public void onCloseStage(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "FinishStage");
        json.put("numeEtapa", stageChoice.getValue());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error closing stage");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            closedStage.setText("Stage couldn't be closed");
            closedStage.setVisible(true);
        }
        else
        {
            javafx.collections.ObservableList obsArrayList = stageChoice.getItems();
            obsArrayList.remove(stageChoice.getValue());
            stageChoice.setItems(obsArrayList);
            closedStage.setText("Stage closed successfully");
            closedStage.setVisible(true);
        }
    }

    @FXML
    public void onSendMessage(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "SaveMessage");
        json.put("message", messageBodyField.getText());
        json.put("username", usernameToSendField.getText());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Error sending the message");
        }
        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if(receivedJSON.get("status_code").toString().equals("failure"))
        {
            messageSentCheck.setText("Message was not sent");
            messageSentCheck.setVisible(true);
            return;
        }
        messageSentCheck.setText("Message was sent");
        messageSentCheck.setVisible(true);
    }
}
