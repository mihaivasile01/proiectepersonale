package com.example.demo;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import socket.Client;
import org.json.JSONObject;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainController {
    @FXML
    private Text loginText;

    @FXML
    private Text inputUsernameText;

    @FXML
    private Button loginButton;

    @FXML
    private TextField loginField;

    @FXML
    private Text errorText;

    @FXML
    public void onLoginButtonClick(MouseEvent mouseEvent) throws IOException {
        Client client = Client.INSTANCE;
        JSONObject json = new JSONObject();
        json.put("command", "LoginRequest");
        json.put("username", loginField.getText());
        ObjectOutputStream os = client.getOutputStream();
        ObjectInputStream is = client.getInputStream();
        try {
            os.writeObject(json.toString());
        } catch (Exception e) {
            System.out.println("Login Error");
        }

        String received;
        JSONObject receivedJSON = null;
        try {
            received = is.readObject().toString();
            receivedJSON = new JSONObject(received.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        if (receivedJSON.get("resultExecution").toString().equals("success")) {
            client.setName(loginField.getText());
            FXMLLoader fxmlLoader;
            if(receivedJSON.get("isAdmin").toString().equals("true"))
            {
                fxmlLoader = new FXMLLoader(MainController.class.getResource("admin-panel.fxml"));
            }
            else
            {
                fxmlLoader = new FXMLLoader(MainController.class.getResource("user-panel.fxml"));
            }
            Parent submitted = (Parent) fxmlLoader.load();

            Window window = ((Node) mouseEvent.getTarget()).getScene().getWindow();
            double height = 800;
            double width = 800;
            double x = window.getX();
            double y = window.getY();
            boolean isFullscreen = ((Stage) window).isFullScreen();

            Scene scene = new Scene(submitted);
            Stage appStage = (Stage) window;
            appStage.setScene(scene);

            appStage.setHeight(height);
            appStage.setWidth(width);
            appStage.setX(x);
            appStage.setY(y);
            appStage.setFullScreen(isFullscreen);

            appStage.show();
            return;
        }
        errorText.setVisible(true);
    }
}