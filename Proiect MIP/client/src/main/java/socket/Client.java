package socket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public enum Client implements Runnable{
    INSTANCE;

    public static final int PORT = 6543;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Socket getSocket() {
        return socket;
    }

    public ObjectInputStream getInputStream() {
        return inputStream;
    }

    public ObjectOutputStream getOutputStream() {
        return outputStream;
    }

    private Socket socket = null;
    private ObjectInputStream inputStream = null;
    private ObjectOutputStream outputStream = null;
    private String name;

    Client() {
        try {
            socket = new Socket("localhost", PORT);
            this.outputStream = new ObjectOutputStream(socket.getOutputStream());
            this.inputStream = new ObjectInputStream(socket.getInputStream());
        } catch(Exception e) {
            System.out.println("Connection error");
        }
    }

    public void run() {
        System.out.println("Client socket running");
        //For receiving data
        boolean isClose = false;
        while (!isClose) {

        }
        try {
            socket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
