﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Model
{
    public class UserProfileProvider : INotifyPropertyChanged
    {
        public string UserName { get; set; }
        public string ImagePath { get; set; }

        public UserProfileProvider(string userName, string imagePath)
        {
            UserName = userName;
            ImagePath = imagePath;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
