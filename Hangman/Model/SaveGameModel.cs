﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Model
{
    class SaveGameModel
    {
        public string CurrentWordHidden { get; set; }
        public string CurrentWord { get; set; }
        public string CurrentCategory { get; set; }
        public List<string> KeysPressed { get; set; }
        public string CurrentLevel { get; set; }
        public string CurrentPlayerName { get; set; } 
        public string CurrentPlayerImageUri { get; set; }
        public int CounterLives { get; set; }
        public string TimerLeft { get; set; }

        
        public SaveGameModel(string PlayerName, string PlayerImageUri, string CurrentCategory, string CurrentLevel,
            string CurrentWord, string CurrentHiddenWord, List<string> KeysPressed, int CounterLives, string timerLeft)
        {
            this.CurrentPlayerName = PlayerName;
            this.CurrentPlayerImageUri = PlayerImageUri;
            this.CurrentCategory = CurrentCategory;
            this.CurrentLevel = CurrentLevel;
            this.CurrentWord = CurrentWord;
            this.CurrentWordHidden = CurrentHiddenWord;
            this.KeysPressed = KeysPressed;
            this.CounterLives = CounterLives;
            this.TimerLeft = timerLeft;
        }
    }
}
