﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Model
{
    class UserStatisticModel
    {
        public string UserName { get; set; }
        public int LoseCount { get; set; }
        public int AllCategoriesWin { get; set; }
        public int CarsWin { get; set; }
        public int FlowersWin { get; set; }
        public int MoviesWin { get; set; }
        public int RiversWin { get; set; }

        [JsonConstructor]
        public UserStatisticModel(string userName, int loseCount, int allCategoriesWin, int carsWin, int flowersWin, int moviesWin, int riversWin)
        {
            UserName = userName;
            LoseCount = loseCount;
            AllCategoriesWin = allCategoriesWin;
            CarsWin = carsWin;
            FlowersWin = flowersWin;
            MoviesWin = moviesWin;
            RiversWin = riversWin;
        }

        public UserStatisticModel(string userName)
        {
            UserName = userName;
            LoseCount = 0;
            AllCategoriesWin = 0;
            CarsWin = 0;
            FlowersWin = 0;
            MoviesWin = 0;
            RiversWin = 0;
        }
    }
}
