﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Model
{
    class GameDataProvider : INotifyPropertyChanged
    {
        private int _counterLives;
        public int CounterLives
        {
            get { return _counterLives; }
            set
            {
                _counterLives = value;
                OnPropertyChanged("CounterLives");
            }
        }
        private string _life6ImageUri;
        public string Life6ImageUri
        {
            get { return _life6ImageUri; }
            set
            {
                _life6ImageUri = value;
                OnPropertyChanged("Life6ImageUri");
            }
        }

        private string _life5ImageUri;
        public string Life5ImageUri
        {
            get { return _life5ImageUri; }
            set
            {
                _life5ImageUri = value;
                OnPropertyChanged("Life5ImageUri");
            }
        }

        private string _life4ImageUri;
        public string Life4ImageUri
        {
            get { return _life4ImageUri; }
            set
            {
                _life4ImageUri = value;
                OnPropertyChanged("Life4ImageUri");
            }
        }

        private string _life3ImageUri;
        public string Life3ImageUri
        {
            get { return _life3ImageUri; }
            set
            {
                _life3ImageUri = value;
                OnPropertyChanged("Life3ImageUri");
            }
        }

        private string _life2ImageUri;
        public string Life2ImageUri
        {
            get { return _life2ImageUri; }
            set
            {
                _life2ImageUri = value;
                OnPropertyChanged("Life2ImageUri");
            }
        }

        private string _life1ImageUri;
        public string Life1ImageUri
        {
            get { return _life1ImageUri; }
            set
            {
                _life1ImageUri = value;
                OnPropertyChanged("Life1ImageUri");
            }
        }

        private string _currentPlayerName;
        public string CurrentPlayerName
        {
            get { return _currentPlayerName; }
            set
            {
                _currentPlayerName = value;
                OnPropertyChanged("CurrentPlayerName");
            }
        }

        private string _currentLevel;
        public string CurrentLevel
        {
            get { return _currentLevel; }
            set
            {
                _currentLevel = value;
                OnPropertyChanged("CurrentLevel");
            }
        }

        private string _currentPlayerImageUri;
        public string CurrentPlayerImageUri
        {
            get { return _currentPlayerImageUri; }
            set
            {
                _currentPlayerImageUri = value;
                OnPropertyChanged("CurrentPlayerImageUri");
            }
        }

        private string _currentHangmanUri;
        public string CurrentHangmanUri
        {
            get { return _currentHangmanUri; }
            set
            {
                _currentHangmanUri = value;
                OnPropertyChanged("CurrentHangmanUri");
            }
        }

        private string _currentWordHidden;
        public string CurrentWordHidden
        {
            get { return _currentWordHidden; }
            set
            {
                _currentWordHidden = value;
                OnPropertyChanged("CurrentWordHidden");
            }
        }
        private string _currentWord;
        public string CurrentWord
        {
            get { return _currentWord; }
            set
            {
                _currentWord = value;
                OnPropertyChanged("CurrentWord");
            }
        }

        private string _currentCategory;
        public string CurrentCategory
        {
            get { return _currentCategory; }
            set
            {
                _currentCategory = value;
                OnPropertyChanged("CurrentCategory");
            }
        }

        private List<string> _keysPressed;
        public List<string> KeysPressed
        {
            get { return _keysPressed; }
            set
            {
                _keysPressed = value;
                OnPropertyChanged("KeysPressed");
            }
        }

        public GameDataProvider()
        {
            CounterLives = 6;
            CurrentCategory = "All categories:";
            KeysPressed = new List<string>();
            CurrentLevel = "1";
        }

        public GameDataProvider ResetGame(string inputCategory, string inputLevel)
        {
            CounterLives = 6;
            CurrentCategory = inputCategory;
            KeysPressed = new List<string>();
            CurrentLevel = inputLevel;
            return this;
        }

        public GameDataProvider LoadGame(string CurrentCategory, string CurrentLevel,string CurrentWord, string CurrentHiddenWord, List<string> KeysPressed, int CounterLives)
        {
            this.CounterLives = CounterLives;
            this.CurrentCategory = CurrentCategory;
            this.CurrentLevel = CurrentLevel;
            this.CurrentWord = CurrentWord;
            this.CurrentWordHidden = CurrentHiddenWord;
            this.KeysPressed = KeysPressed;
            return this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
