﻿using Hangman.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.View
{
    /// <summary>
    /// Interaction logic for StatisticsPage.xaml
    /// </summary>
    public partial class StatisticsPage : Window
    { 
        public StatisticsPage()
        {
            InitializeComponent();
            CreateTable();
        }

        public void CreateTable()
        {
            int numberOfColumns = 7;
            for (int x = 0; x < numberOfColumns; x++)
            {
                TableStatistics.Columns.Add(new TableColumn());
            }

            TableStatistics.RowGroups.Add(new TableRowGroup());
            TableStatistics.RowGroups[0].Rows.Add(new TableRow());
            TableRow currentRow = TableStatistics.RowGroups[0].Rows[0];

            //Table Title
            currentRow.FontSize = 24;
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Statistica utilizatori"))));
            currentRow.Cells[0].ColumnSpan = 7;

            //Header Row
            TableStatistics.RowGroups[0].Rows.Add(new TableRow());
            currentRow = TableStatistics.RowGroups[0].Rows[1];
            currentRow.FontSize = 18;

            Paragraph tableHeader = new Paragraph(new Run("Username"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("No. Loses"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("Wins All"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("Wins Cars"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("Wins Flowers"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("Wins Movies"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            tableHeader = new Paragraph(new Run("Wins Rivers"));
            tableHeader.TextAlignment = TextAlignment.Center;
            currentRow.Cells.Add(new TableCell(tableHeader));

            using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
            {
                string line = null;
                int currentUserNo = 2;
                while ((line = sr.ReadLine()) != null)
                {
                    UserStatisticModel currentUser = JsonConvert.DeserializeObject<UserStatisticModel>(line);

                    TableStatistics.RowGroups[0].Rows.Add(new TableRow());
                    currentRow = TableStatistics.RowGroups[0].Rows[currentUserNo];
                    currentRow.FontSize = 14;

                    Paragraph tableUserEntry = new Paragraph(new Run(currentUser.UserName));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.LoseCount.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.AllCategoriesWin.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.CarsWin.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.FlowersWin.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.MoviesWin.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    tableUserEntry = new Paragraph(new Run(currentUser.RiversWin.ToString()));
                    tableUserEntry.TextAlignment = TextAlignment.Center;
                    currentRow.Cells.Add(new TableCell(tableUserEntry));

                    currentUserNo++;
                }
            }

        }
    }
}
