﻿using Hangman.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Hangman.View
{
    /// <summary>
    /// Interaction logic for GameInterface.xaml
    /// </summary>
    public partial class GameInterface : Window
    {
        DispatcherTimer timer;
        private int time = 60;

        public GameInterface(UserProfileProvider userProfile)
        {
            InitializeComponent();
            PlayerName.Text = userProfile.UserName;
            PlayerImage.Source = new BitmapImage(new Uri(userProfile.ImagePath));
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Ticker;
            timer.Start();
        }

        private void Ticker(object sender, EventArgs e)
        {
            time--;
            if (time > 0)
            {
                TimerText.Text = string.Format("{0}:{1}", time / 60, time % 60);
            }
            else
            {
                timer.Stop();
                if (LevelAsString.Text != "5")
                {

                    LoseGameWindow lose = new LoseGameWindow();
                    lose.Show();
                    LockButtons();

                    List<string> userStatistics = new List<string>();
                    using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
                    {
                        string line = null;
                        while ((line = sr.ReadLine()) != null)
                        {
                            userStatistics.Add(line);
                        }
                    }
                    using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
                    {
                        for (int i = 0; i < userStatistics.Count; i++)
                        {
                            if (!userStatistics[i].Contains(PlayerName.Text))
                                sw.WriteLine(userStatistics[i]);
                            else
                            {
                                UserStatisticModel currentUser = JsonConvert.DeserializeObject<UserStatisticModel>(userStatistics[i]);
                                currentUser.LoseCount++;
                                string newStatistic = JsonConvert.SerializeObject(currentUser);
                                sw.WriteLine(newStatistic);
                            }
                        }
                    }
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem itemChecked = (MenuItem)sender;
            MenuItem itemParent = (MenuItem)itemChecked.Parent;

            foreach (MenuItem item in itemParent.Items)
            {
                if (item == itemChecked) continue;

                item.IsChecked = false;
            }
            CategoryRound.Text = itemChecked.Header.ToString() +":";
            ResetButtons();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button buttonPressed = (Button)sender;
            buttonPressed.IsEnabled = false;
        }

        public void ResetButtons()
        {
            ButtonQ.IsEnabled = true;
            ButtonW.IsEnabled = true;
            ButtonE.IsEnabled = true;
            ButtonR.IsEnabled = true;
            ButtonT.IsEnabled = true;
            ButtonY.IsEnabled = true;
            ButtonU.IsEnabled = true;
            ButtonI.IsEnabled = true;
            ButtonO.IsEnabled = true;
            ButtonP.IsEnabled = true;

            ButtonA.IsEnabled = true;
            ButtonS.IsEnabled = true;
            ButtonD.IsEnabled = true;
            ButtonF.IsEnabled = true;
            ButtonG.IsEnabled = true;
            ButtonH.IsEnabled = true;
            ButtonJ.IsEnabled = true;
            ButtonK.IsEnabled = true;
            ButtonL.IsEnabled = true;

            ButtonZ.IsEnabled = true;
            ButtonX.IsEnabled = true;
            ButtonC.IsEnabled = true;
            ButtonV.IsEnabled = true;
            ButtonB.IsEnabled = true;
            ButtonN.IsEnabled = true;
            ButtonM.IsEnabled = true;
        }

        public void LockButtons()
        {
            ButtonQ.IsEnabled = false;
            ButtonW.IsEnabled = false;
            ButtonE.IsEnabled = false;
            ButtonR.IsEnabled = false;
            ButtonT.IsEnabled = false;
            ButtonY.IsEnabled = false;
            ButtonU.IsEnabled = false;
            ButtonI.IsEnabled = false;
            ButtonO.IsEnabled = false;
            ButtonP.IsEnabled = false;

            ButtonA.IsEnabled = false;
            ButtonS.IsEnabled = false;
            ButtonD.IsEnabled = false;
            ButtonF.IsEnabled = false;
            ButtonG.IsEnabled = false;
            ButtonH.IsEnabled = false;
            ButtonJ.IsEnabled = false;
            ButtonK.IsEnabled = false;
            ButtonL.IsEnabled = false;

            ButtonZ.IsEnabled = false;
            ButtonX.IsEnabled = false;
            ButtonC.IsEnabled = false;
            ButtonV.IsEnabled = false;
            ButtonB.IsEnabled = false;
            ButtonN.IsEnabled = false;
            ButtonM.IsEnabled = false;
        }

        private void CheckIfPressed()
        {
            if(KeysPressedHidden.Text.Contains("Q"))
                ButtonQ.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("W"))
                ButtonW.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("E"))
                ButtonE.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("R"))
                ButtonR.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("T"))
                ButtonT.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("Y"))
                ButtonY.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("U"))
                ButtonU.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("I"))
                ButtonI.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("O"))
                ButtonO.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("P"))
                ButtonP.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("A"))
                ButtonA.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("S"))
                ButtonS.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("D"))
                ButtonD.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("F"))
                ButtonF.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("G"))
                ButtonG.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("H"))
                ButtonH.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("J"))
                ButtonJ.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("K"))
                ButtonK.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("L"))
                ButtonL.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("Z"))
                ButtonZ.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("X"))
                ButtonX.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("C"))
                ButtonC.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("V"))
                ButtonV.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("B"))
                ButtonB.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("N"))
                ButtonN.IsEnabled = false;
            if (KeysPressedHidden.Text.Contains("M"))
                ButtonM.IsEnabled = false;
        }

        private void New_Game_Click(object sender, RoutedEventArgs e)
        {
            ResetButtons();
            time = 60;
            if (!timer.IsEnabled)
                timer.Start();
        }

        private void Next_Level_Click(object sender, RoutedEventArgs e)
        {
            ResetButtons();
            time = 60;
        }

        private void Load_Game_Click(object sender, RoutedEventArgs e)
        {
            ResetButtons();
            timer.Stop();
        }

        private void Calibrate_Keys(object sender, RoutedEventArgs e)
        {
            CheckIfPressed();
            time = Int32.Parse(TimerText.Text[0].ToString()) * 60 + Int32.Parse(TimerText.Text[2].ToString()) * 10 + Int32.Parse(TimerText.Text[3].ToString());
            timer.Start();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
