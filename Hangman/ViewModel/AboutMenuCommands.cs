﻿using Hangman.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    class AboutMenuCommands
    {
        private ICommand m_aboutPage;

        private void AboutPage(object parameter)
        {
            var aboutPage = new About();
            aboutPage.Show();
        }
        public ICommand About
        {
            get
            {
                if (m_aboutPage == null)
                    m_aboutPage = new RelayCommand(AboutPage);
                return m_aboutPage;
            }
        }
    }
}
