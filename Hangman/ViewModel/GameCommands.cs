﻿using Hangman.Model;
using Hangman.View;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    class GameCommands : INotifyPropertyChanged
    {
        private ICommand m_saveGame;
        private ICommand m_newGame;
        private ICommand m_loadGame;
        private ICommand m_pressKey;
        private ICommand m_nextLevel;
        private ICommand m_calibrateKeys;
        private ICommand m_showStatistics;
        public AboutMenuCommands AboutTab { get; set; }
        public GameDataProvider GameData { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private string _timerLeft;
        public string TimerLeft
        {
            get { return _timerLeft; }
            set
            {
                _timerLeft = value;
                OnPropertyChanged("TimerLeft");
            }
        }

        private string _NextLevelButtonVisibility = "Hidden";
        public string NextLevelButtonVisibility
        {
            get { return _NextLevelButtonVisibility; }
            set
            {
                _NextLevelButtonVisibility = value;
                OnPropertyChanged("NextLevelButtonVisibility");
            }
        }

        private string _CalibrateKeysVisibility = "Hidden";
        public string CalibrateKeysVisibility
        {
            get { return _CalibrateKeysVisibility; }
            set
            {
                _CalibrateKeysVisibility = value;
                OnPropertyChanged("CalibrateKeysVisibility");
            }
        }

        private string _KeysPressedString;
        public string KeysPressedString 
        {
            get { return _KeysPressedString; }
            set
            {
                _KeysPressedString = value;
                OnPropertyChanged("KeysPressedString");
            }
        }

        private void SaveGameOption(object parameter)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Json Files (*.json)|*.json|All files (*.*)|*.*";
            string CombinedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Resources\\SaveGames");
            saveFileDialog.InitialDirectory = System.IO.Path.GetFullPath(CombinedPath);
            if (saveFileDialog.ShowDialog() == true)
            {
                SaveGameModel userToSave = new SaveGameModel(GameData.CurrentPlayerName, GameData.CurrentPlayerImageUri, GameData.CurrentCategory, GameData.CurrentLevel, GameData.CurrentWord,
                    GameData.CurrentWordHidden, GameData.KeysPressed, GameData.CounterLives, TimerLeft);
                string output = JsonConvert.SerializeObject(userToSave);
                using (StreamWriter sw = File.CreateText(saveFileDialog.FileName))
                {
                    sw.Write(output);
                }
            }
        }

        private void LoadGameFunction(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Json Files (*.json)|*.json|All files (*.*)|*.*";
            string CombinedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Resources\\SaveGames");
            openFileDialog.InitialDirectory = System.IO.Path.GetFullPath(CombinedPath);
            if(openFileDialog.ShowDialog()==true)
            {
                using (StreamReader sr = File.OpenText(openFileDialog.FileName))
                {
                    string currentObject = sr.ReadLine();
                    SaveGameModel readSave = JsonConvert.DeserializeObject<SaveGameModel>(currentObject);
                    if(readSave.CurrentPlayerName!= GameData.CurrentPlayerName)
                    {
                        AccessDeniedPage accessDeniedPage = new AccessDeniedPage();
                        accessDeniedPage.Show();
                    }
                    else
                    {
                        GameData = GameData.LoadGame(readSave.CurrentCategory, readSave.CurrentLevel, readSave.CurrentWord, 
                            readSave.CurrentWordHidden, readSave.KeysPressed, readSave.CounterLives);
                        LoadHangman();
                        LoadHearts();
                        TimerLeft = readSave.TimerLeft;
                        KeysPressedString = String.Join(",", GameData.KeysPressed);
                        CalibrateKeysVisibility = "Visible";
                    }
                }
            }
        }

        private void WonGame()
        {
            if (GameData.CurrentLevel == "5")
            {
                WonGameWindow wonGame = new WonGameWindow();
                wonGame.Show();

                List<string> userStatistics = new List<string>();
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        userStatistics.Add(line);
                    }
                }
                using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
                {
                    for (int i = 0; i < userStatistics.Count; i++)
                    {
                        if (!userStatistics[i].Contains(GameData.CurrentPlayerName))
                            sw.WriteLine(userStatistics[i]);
                        else
                        {
                            UserStatisticModel currentUser = JsonConvert.DeserializeObject<UserStatisticModel>(userStatistics[i]);
                            if (GameData.CurrentCategory == "All Categories:")
                                currentUser.AllCategoriesWin++;
                            if (GameData.CurrentCategory == "Cars:")
                                currentUser.CarsWin++;
                            if (GameData.CurrentCategory == "Flowers:")
                                currentUser.FlowersWin++;
                            if (GameData.CurrentCategory == "Movies:")
                                currentUser.MoviesWin++;
                            if (GameData.CurrentCategory == "Rivers:")
                                currentUser.RiversWin++;
                            string newStatistic = JsonConvert.SerializeObject(currentUser);
                            sw.WriteLine(newStatistic);
                        }
                    }
                }
            }
            else
            {
                NextLevelButtonVisibility = "Visible";
            }
        }

        private void NextLevelFunction(object parameter)
        {
            int numberLvl = Int32.Parse(GameData.CurrentLevel) + 1;
            GameData.CurrentLevel = numberLvl.ToString();
            GameData = GameData.ResetGame(GameData.CurrentCategory, GameData.CurrentLevel);
            LoadHangman();
            LoadHearts();
            LoadWordForHangman();
            NextLevelButtonVisibility = "Hidden";
        }

        private void LoseGame()
        {
            LoseGameWindow loseGameWindow = new LoseGameWindow();
            loseGameWindow.Show();

            List<string> userStatistics = new List<string>();
            using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    userStatistics.Add(line);
                }
            }
            using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
            {
                for (int i = 0; i < userStatistics.Count; i++)
                {
                    if (!userStatistics[i].Contains(GameData.CurrentPlayerName))
                        sw.WriteLine(userStatistics[i]);
                    else
                    {
                        UserStatisticModel currentUser = JsonConvert.DeserializeObject<UserStatisticModel>(userStatistics[i]);
                        currentUser.LoseCount++;
                        string newStatistic = JsonConvert.SerializeObject(currentUser);
                        sw.WriteLine(newStatistic);
                    }
                }
            }
        }

        private void PressKeyButton(object parameter)
        {
            string KeyPressed = (string)parameter;
            GameData.KeysPressed.Add(KeyPressed);
            if(GameData.CurrentWordHidden.Contains(KeyPressed))
            {
                char[] newCurrentWord = GameData.CurrentWord.ToCharArray();
                for (int i = 0; i < GameData.CurrentWord.Length; i=i+2)
                {
                    if (GameData.CurrentWordHidden[i/2].ToString() == KeyPressed)
                        newCurrentWord[i] = GameData.CurrentWordHidden[i/2];
                    else
                        newCurrentWord[i] = GameData.CurrentWord[i];
                }
                string aux = new string(newCurrentWord);
                GameData.CurrentWord = aux;
                if (!GameData.CurrentWord.Contains("_"))
                    WonGame();
            }
            else
            {
                GameData.CounterLives--;
                LoadHearts();
                LoadHangman();
                if (GameData.CounterLives == 0)
                    LoseGame();
            }
            KeysPressedString = String.Join(",", GameData.KeysPressed);
        }

        private void LoadWordForHangman()
        {
            List<string> possibleWords = new List<string>();
            if(GameData.CurrentCategory== "All categories:")
            {
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\Categories\\All.txt"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        possibleWords.Add(line);
                    }
                }
            }
            if (GameData.CurrentCategory == "Cars:")
            {
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\Categories\\Cars.txt"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        possibleWords.Add(line);
                    }
                }
            }
            if (GameData.CurrentCategory == "Flowers:")
            {
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\Categories\\Flowers.txt"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        possibleWords.Add(line);
                    }
                }
            }
            if (GameData.CurrentCategory == "Movies:")
            {
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\Categories\\Movies.txt"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        possibleWords.Add(line);
                    }
                }
            }
            if (GameData.CurrentCategory == "Rivers:")
            {
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\Categories\\Rivers.txt"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        possibleWords.Add(line);
                    }
                }
            }
            Random random = new Random();
            int chosenWordPos = random.Next(0, possibleWords.Count-1);
            GameData.CurrentWordHidden = possibleWords[chosenWordPos];
            string hideWord = "";
            for (int i = 0; i < GameData.CurrentWordHidden.Length; i++)
            {
                if (Char.IsLetter(GameData.CurrentWordHidden[i]))
                    hideWord += "_ ";
                else
                    hideWord += GameData.CurrentWordHidden[i];
            }
            GameData.CurrentWord = hideWord;
        }

        private void LoadHangman()
        {
            switch(GameData.CounterLives)
            {
                case 0:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman6.png";
                    break;
                case 1:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman5.png";
                    break;
                case 2:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman4.png";
                    break;
                case 3:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman3.png";
                    break;
                case 4:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman2.png";
                    break;
                case 5:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman1.png";
                    break;
                case 6:
                    GameData.CurrentHangmanUri = "..\\..\\Images\\GameImages\\hangman0.png";
                    break;

            }
        }

        private void NewGameFunction(object parameter)
        {
            if (GameData.CurrentLevel != "1")
            {
                List<string> userStatistics = new List<string>();
                using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
                {
                    string line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        userStatistics.Add(line);
                    }
                }
                using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
                {
                    for (int i = 0; i < userStatistics.Count; i++)
                    {
                        if (!userStatistics[i].Contains(GameData.CurrentPlayerName))
                            sw.WriteLine(userStatistics[i]);
                        else
                        {
                            UserStatisticModel currentUser = JsonConvert.DeserializeObject<UserStatisticModel>(userStatistics[i]);
                            currentUser.LoseCount++;
                            string newStatistic = JsonConvert.SerializeObject(currentUser);
                            sw.WriteLine(newStatistic);
                        }
                    }
                }
            }

            GameData.CurrentLevel = "1";
            GameData.CounterLives = 6;
            LoadHangman();
            LoadHearts();
            LoadWordForHangman();
        }

        private void CalibrateKeysFunction(object parameter)
        {
            CalibrateKeysVisibility = "Hidden";
        }

        private void ShowStatisticsFunction(object parameter)
        {
            StatisticsPage statisticsPage = new StatisticsPage();
            statisticsPage.Show();
        }

        private void LoadHearts()
        {
            switch(GameData.CounterLives)
            {
                case 0:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 1:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 2:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 3:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 4:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 5:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\EmptyLife.png";
                    break;
                case 6:
                    GameData.Life1ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life2ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life3ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life4ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life5ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    GameData.Life6ImageUri = "..\\..\\Images\\GameImages\\FullLife.png";
                    break;
            }
        }


        public ICommand NewGame
        {
            get
            {
                if (m_newGame == null)
                    m_newGame = new RelayCommand(NewGameFunction);
                return m_newGame;
            }
        }
        public ICommand SaveGame
        {
            get
            {
                if (m_saveGame == null)
                    m_saveGame = new RelayCommand(SaveGameOption);
                return m_saveGame;
            }
        }

        public ICommand PressKey
        {
            get
            {
                if (m_pressKey == null)
                    m_pressKey = new RelayCommand(PressKeyButton);
                return m_pressKey;
            }
        }

        public ICommand NextLevel
        {
            get
            {
                if (m_nextLevel == null)
                    m_nextLevel = new RelayCommand(NextLevelFunction);
                return m_nextLevel;
            }
        }

        public ICommand LoadGame
        {
            get
            {
                if (m_loadGame == null)
                    m_loadGame = new RelayCommand(LoadGameFunction);
                return m_loadGame;
            }
        }
        public ICommand CalibrateKeys
        {
            get
            {
                if (m_calibrateKeys == null)
                    m_calibrateKeys = new RelayCommand(CalibrateKeysFunction);
                return m_calibrateKeys;
            }
        }
        public ICommand ShowStatistics
        {
            get
            {
                if (m_showStatistics == null)
                    m_showStatistics = new RelayCommand(ShowStatisticsFunction);
                return m_showStatistics;
            }
        }

        public GameCommands()
        {
            AboutTab = new AboutMenuCommands();
            GameData = new GameDataProvider();
            LoadHearts();
            LoadHangman();
            LoadWordForHangman();
        }
    }
}
