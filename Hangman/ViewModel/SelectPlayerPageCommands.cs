﻿using Hangman.Model;
using Hangman.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    public class SelectPlayerPageCommands : INotifyPropertyChanged
    {
        private ICommand m_newUser;
        private ICommand m_cancel;
        private ICommand m_playGame;
        private ICommand m_deleteUser;

        private string _currentImageProfileUri;
        public string CurrentImageProfileUri
        {
            get { return _currentImageProfileUri; }
            set
            {
                _currentImageProfileUri = value;
                OnPropertyChanged("CurrentImageProfileUri");
            }
        }

        private UserProfileProvider _currentSelectedProfile;
        public UserProfileProvider CurrentSelectedProfile
        {
            get { return _currentSelectedProfile; }
            set
            {
                _currentSelectedProfile = value;
                CurrentImageProfileUri = _currentSelectedProfile.ImagePath;
                OnPropertyChanged("CurrentSelectedProfile");
                IsPlayButtonOpen = true;
                IsDeleteUserButtonOpen = true;
            }
        }
        private bool _isPlayButtonOpen = false;
        public bool IsPlayButtonOpen 
        { 
            get { return _isPlayButtonOpen; }
            set
            {
                _isPlayButtonOpen = value;
                OnPropertyChanged("IsPlayButtonOpen");
            }
        }
        private bool _isDeleteUserButtonOpen = false;
        public bool IsDeleteUserButtonOpen 
        { 
            get { return _isDeleteUserButtonOpen; }
            set
            {
                _isDeleteUserButtonOpen = value;
                OnPropertyChanged("IsDeleteUserButtonOpen");
            } 
        }
        public ObservableCollection<UserProfileProvider> UserProfiles { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void CancelButton(object parameter)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void PlayGameFunction(object parameter)
        {
            GameInterface gameInterface = new GameInterface(CurrentSelectedProfile);
            gameInterface.Show();
            
        }

        private void DeleteUserButton(object parameter)
        {
            List<string> userStatistics = new List<string>();
            using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserStatistics.json"))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    userStatistics.Add(line);
                }
            }
            using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
            {
                for (int i = 0; i < userStatistics.Count; i++)
                {
                    if (!userStatistics[i].Contains(CurrentSelectedProfile.UserName))
                        sw.WriteLine(userStatistics[i]);
                }
            }

            List<string> userProfiles = new List<string>();
            using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserProfiles.json"))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    userProfiles.Add(line);
                }
            }
            using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserProfiles.json"))
            {
                for (int i = 0; i < userProfiles.Count; i++)
                {
                    if (!userProfiles[i].Contains(CurrentSelectedProfile.UserName))
                        sw.WriteLine(userProfiles[i]);
                }
            }

            foreach (string file in Directory.EnumerateFiles("..\\..\\Resources\\SaveGames", "*.json"))
            {
                string contents = File.ReadAllText(file);
                if (contents.Contains(CurrentSelectedProfile.UserName))
                    File.Delete(file);
            }
            UserProfileProvider userToDelete = CurrentSelectedProfile;
            CurrentSelectedProfile = UserProfiles[0];
            UserProfiles.Remove(userToDelete);
        }

        private void NewUserButton(object parameter)
        {
            NewUserPage newUserPage = new NewUserPage(this);
            newUserPage.Show();
        }

        private void LoadUserProfiles()
        {
            if (!File.Exists("..\\..\\Resources\\UserProfiles.json"))
            {
                File.Create("..\\..\\Resources\\UserProfiles.json");
                return;
            }
            using (StreamReader sr = File.OpenText("..\\..\\Resources\\UserProfiles.json"))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    UserProfileProvider currentUser = JsonConvert.DeserializeObject<UserProfileProvider>(line);
                    UserProfiles.Add(currentUser);
                }
            }
        }

        public ICommand PlayHangman
        {
            get
            {
                if (m_playGame == null)
                    m_playGame = new RelayCommand(PlayGameFunction);
                return m_playGame;
            }
        }
        public ICommand NewUser
        {
            get
            {
                if (m_newUser == null)
                    m_newUser = new RelayCommand(NewUserButton);
                return m_newUser;
            }
        }

        public ICommand DeleteUser
        {
            get
            {
                if (m_deleteUser == null)
                    m_deleteUser = new RelayCommand(DeleteUserButton);
                return m_deleteUser;
            }
        }
        public ICommand Cancel
        {
            get
            {
                if (m_cancel == null)
                    m_cancel = new RelayCommand(CancelButton);
                return m_cancel;
            }
        }

        public SelectPlayerPageCommands()
        {
            UserProfiles = new ObservableCollection<UserProfileProvider>();
            LoadUserProfiles();
        }
    }
}
