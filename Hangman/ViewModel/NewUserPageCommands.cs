﻿using Hangman.Model;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    class NewUserPageCommands : INotifyPropertyChanged
    {
        private ICommand m_selectImage;
        private ICommand m_createUser;

        private string _userNameInput;
        public string UserNameInput 
        { 
            get { return _userNameInput; }
            set
            {
                _userNameInput = value;
                OnPropertyChanged("UserNameInput");
            } 
        }

        private string _selectedImageUri;
        public string SelectedImageUri
        {
            get { return _selectedImageUri; }
            set
            {
                _selectedImageUri = value;
                OnPropertyChanged("SelectedImageUri");
            }
        }

        private void SelectImage(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";
            openFileDialog.Title = "Select Image";
            string CombinedPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\Images\\ProfileImages");
            openFileDialog.InitialDirectory = System.IO.Path.GetFullPath(CombinedPath);
            if (openFileDialog.ShowDialog() == true)
                SelectedImageUri = openFileDialog.FileName;
        }

        private void CreateUser(object parameter)
        {
            SelectPlayerPageCommands playerProfiles = (SelectPlayerPageCommands)parameter;
            UserProfileProvider toAddUser = new UserProfileProvider(UserNameInput, SelectedImageUri);
            playerProfiles.UserProfiles.Add(toAddUser);
            string output = JsonConvert.SerializeObject(toAddUser);
            if (!File.Exists("..\\..\\Resources\\UserProfiles.json"))
            {
                using (StreamWriter sw = File.CreateText("..\\..\\Resources\\UserProfiles.json"))
                {
                    sw.Write(output);
                }
                return;
            }
            using (StreamWriter sw = File.AppendText("..\\..\\Resources\\UserProfiles.json"))
            {
                sw.Write(output);
            }

            UserStatisticModel userStats = new UserStatisticModel(toAddUser.UserName);
            string outputStats = JsonConvert.SerializeObject(userStats);
            if (!File.Exists("..\\..\\Resources\\UserStatistics.json"))
            {
                using (StreamWriter swStats = File.CreateText("..\\..\\Resources\\UserStatistics.json"))
                {
                    swStats.Write(outputStats);
                }
                return;
            }
            using (StreamWriter swStats = File.AppendText("..\\..\\Resources\\UserStatistics.json"))
            {
                swStats.Write(outputStats);
            }
        }

        public ICommand SelectImageButton
        {
            get
            {
                if (m_selectImage == null)
                    m_selectImage = new RelayCommand(SelectImage);
                return m_selectImage;
            }
        }

        public ICommand CreateUserButton
        {
            get
            {
                if (m_createUser == null)
                    m_createUser = new RelayCommand(CreateUser);
                return m_createUser;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
